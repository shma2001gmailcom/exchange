package org.misha;

import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.LockSupport;

public class TheExchanger<T> implements Exchanger<T> {
    private final AtomicReference<T> valueRef = new AtomicReference<>(null);
    private final AtomicReference<Thread> waiter = new AtomicReference<>(null);

    public void put(T value) {
        while (true) {
            if (valueRef.compareAndSet(null, value)) {
                Thread other = waiter.getAndSet(null);
                if (other != null) {
                    LockSupport.unpark(other);
                }
                return;
            }
            // Busy wait
            LockSupport.parkNanos(1);
        }
    }

    public T get() {
        while (true) {
            T val = valueRef.getAndSet(null);
            if (val != null) {
                return val;
            }
            // Indicate this thread is waiting
            waiter.set(Thread.currentThread());
            if (valueRef.get() == null) {
                LockSupport.parkNanos(1);
            }
        }
    }
}
