package org.misha;

public class Consumer<T> {
    private final Exchanger<T> exchanger;

    public Consumer(Exchanger<T> exchanger) {
        this.exchanger = exchanger;
    }

    public void consume() {
        T t = exchanger.get();
        process(t);
    }

    public void process(T t) {
        //System.out.println(t);
    }
}
