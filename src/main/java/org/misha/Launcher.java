package org.misha;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import static org.misha.Node.randomNode;

//the class is for the profiling
public class Launcher {
    private static final Logger log = Logger.getLogger("Launcher");
    private final Exchanger<Node<Integer>> exchanger;
    private static final int times = 1;

    Launcher() {
        exchanger = new TheExchanger<>();
    }

    private void start(AtomicInteger counter, List<Future<Void>> tasks) {
        try {
            IntStream.range(0, Runtime.getRuntime().availableProcessors() / 2).forEach(index -> {
                Future<Void> supplier = CompletableFuture.supplyAsync(() -> {
                    Producer<Node<Integer>> producer = new Producer<>(exchanger);
                    int i = 0;
                    while (i < times) {
                        int depth = 5;
                        int width = 7;
                        Node<Integer> root = new Node<>(0);
                        producer.produce(randomNode(depth, width, root));
                        ++i;
                        counter.incrementAndGet();
                    }
                    return null;
                });
                Future<Void> receiver = CompletableFuture.supplyAsync(() -> {
                    class Stack {
                        final AtomicInteger i = new AtomicInteger();
                        final LinkedList<Node<Integer>> list = new LinkedList<>();

                        void push(Node<Integer> node) {
                            list.push(node);
                        }

                        Node<Integer> pop() {
                            Node<Integer> pop = list.pop();
                            if (pop.isNotLeaf()) {
                                pop.forEach(list::add);
                            } else {
                                i.incrementAndGet();
                            }
                            return pop;
                        }

                        int scanLeaves() {
                            try {
                                while (!list.isEmpty()) {
                                    System.out.println("        popped: " + pop());
                                }
                                return i.get();
                            } finally {
                                reset();
                            }
                        }

                        void reset() {
                            i.set(0);
                        }
                    }
                    Consumer<Node<Integer>> consumer = new Consumer<>(exchanger) {

                        final Stack stack = new Stack();

                        @Override
                        public void process(Node<Integer> node) {
                            System.out.println("processing: " + node);
                            stack.push(node);
                            System.out.println("total leaves: " + stack.scanLeaves());
                        }
                    };
                    int i = 0;
                    while (i < times) {
                        ++i;
                        consumer.consume();
                        counter.decrementAndGet();
                    }
                    return null;
                });
                tasks.add(supplier);
                tasks.add(receiver);
            });
            tasks.forEach(future -> {
                try {
                    future.get();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    log.log(Level.SEVERE, e::getMessage);
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            tasks.forEach(task -> log.log(Level.SEVERE, () -> "final state: " + task.state()));
        }
    }

    public static void main(String[] args) {
        new Launcher().start(new AtomicInteger(), new ArrayList<>());
    }
}
