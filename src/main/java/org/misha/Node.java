package org.misha;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.Consumer;

public class Node<T> implements Iterable<Node<T>> {
    private final T data;
    private Set<Node<T>> children;

    Node(T data) {
        this.data = data;
    }

    void add(Node<T> child) {
        if (children == null) children = new HashSet<>();
        children.add(child);
    }

    @Override
    public Iterator<Node<T>> iterator() {
        return children.iterator();
    }

    boolean isNotLeaf() {
        return children != null;
    }

    @Override
    public void forEach(Consumer<? super Node<T>> action) {
        children.forEach(action);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(data.toString());
        if (children != null) {
            StringJoiner joiner = new StringJoiner(", ", " -> [", "]");
            for (Node<T> n : children) joiner.add(n.toString());
            sb.append(joiner);
        }
        return sb.toString();
    }

    static Node<Integer> randomNode(int depth, int width, Node<Integer> root) {
        if (depth == 0) return root;
        Node<Integer> tNode;
        while (depth > 0) {
            --depth;
            for (int i = 0; i < width; ++i) {
                tNode = new Node<>(depth);
                randomNode(depth, width, tNode);
                root.add(tNode);
            }
        }
        return root;
    }
}
