package org.misha;

public class Producer<T> {
    private final Exchanger<T> exchanger;

    public Producer(Exchanger<T> exchanger) {
        this.exchanger = exchanger;
    }

    public void produce(T t) {
        exchanger.put(t);
    }
}
