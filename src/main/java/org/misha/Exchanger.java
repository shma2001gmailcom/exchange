package org.misha;

public interface Exchanger<T> {
    void put(T value);

    T get();
}
