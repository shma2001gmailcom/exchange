package org.misha;

import org.junit.jupiter.api.Test;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.misha.Node.randomNode;

class ExchangerTest {
    private static final Logger log = LoggerFactory.getLogger(ExchangerTest.class);
    private final Exchanger<Node<Integer>> exchanger;
    private static final int times = 1;

    ExchangerTest() {
        exchanger = new TheExchanger<>();
    }

    @Test
    void checkManyActorsOnSingleExchanger() {
        log.info(() -> "\n\nthread:" + Thread.currentThread().getName());
        AtomicInteger counter = new AtomicInteger();
        List<Future<Void>> tasks = new ArrayList<>();
        start(counter, tasks);
        assertEquals(0, counter.get());
    }

    private void start(AtomicInteger counter, List<Future<Void>> tasks) {
        try {
            IntStream.range(0, Runtime.getRuntime().availableProcessors() / 2).forEach(index -> {
                Future<Void> supplier = CompletableFuture.supplyAsync(() -> {
                    log.info(() -> "\n\nthread:" + Thread.currentThread().getName());
                    Producer<Node<Integer>> producer = new Producer<>(exchanger);
                    int i = 0;
                    while (i < times) {
                        int depth = 5;
                        int width = 5;
                        Node<Integer> root = new Node<>(0);
                        producer.produce(randomNode(depth, width, root));
                        ++i;
                        counter.incrementAndGet();
                    }
                    return null;
                });
                Future<Void> receiver = CompletableFuture.supplyAsync(() -> {
                    log.info(() -> "\n\nthread:" + Thread.currentThread().getName());
                    class Stack {
                        final AtomicInteger i = new AtomicInteger();
                        final LinkedList<Node<Integer>> list = new LinkedList<>();

                        void push(Node<Integer> node) {
                            list.push(node);
                        }

                        Node<Integer> pop() {
                            Node<Integer> pop = list.pop();
                            if (pop.isNotLeaf()) {
                                pop.forEach(list::add);
                            } else {
                                i.incrementAndGet();
                            }
                            return pop;
                        }

                        int scanLeaves() {
                            try {
                                while (!list.isEmpty()) {
                                    log.info(() -> "        popped: " + pop());
                                }
                                return i.get();
                            } finally {
                                reset();
                            }
                        }

                        void reset() {
                            i.set(0);
                        }
                    }
                    Consumer<Node<Integer>> consumer = new Consumer<>(exchanger) {

                        final Stack stack = new Stack();

                        @Override
                        public void process(Node<Integer> node) {
                            log.info(() -> "processing: "+ node);
                            stack.push(node);
                            log.info(() -> "total leaves: " + stack.scanLeaves());
                        }
                    };
                    int i = 0;
                    while (i < times) {
                        ++i;
                        consumer.consume();
                        counter.decrementAndGet();
                    }
                    return null;
                });
                tasks.add(supplier);
                tasks.add(receiver);
            });
            tasks.forEach(future -> {
                try {
                    future.get();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    log.error(e::getMessage);
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}